#pragma once

#include "utils/Container.hpp"

namespace median_task
{
namespace utils
{
template <typename T>
Container<T> solution(int argc, char** argv);
}
}