#include "utils/Solution.hpp"

#include "utils/Sorting.hpp"
#include "utils/IntigerConverter.hpp"

namespace median_task
{
namespace utils
{
template <typename T>
Container<T> solution(int argc, char** argv)
{
    auto container = Container<int>();
    auto result = Container<float>();

    for (int i = 0; i < argc; ++i)
    {
        if (*argv[i] == 'm')
        {
            median_task::utils::quick_sort(container);
            int n = container.size();
            if (n % 2 == 0)
            {
                result.push_back((container[n / 2 - 1] + container[n / 2]) * 0.5f);
            }
            else
                result.push_back(static_cast<float>(container[n / 2]));
        }
        else if (*argv[i] == 'q')
        {
            break;
        }
        else
        {
            container.push_back(char_to_int(argv[i]));
        }
    }

    return result;
}

template Container<float> solution(int, char**);
}
}