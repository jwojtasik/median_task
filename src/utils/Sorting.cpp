#include "utils/Sorting.hpp"

#include <iostream>

#include "utils/Container.hpp"

namespace
{
template <typename T>
void swap(T& one, T& two)
{
    T tmp = one;
    one = two;
    two = tmp;
}

template <typename T>
int partition(median_task::utils::Container<T>& container, int first, int last)
{
    int pivot = container[last];
    int i = first - 1;

    for (int j = first; j <= last - 1; ++j)
    {
        if (container[j] < pivot)
        {
            if (i != j)
            {
                ++i;
                swap(container[i], container[j]);
            }
        }
    }
    ++i;
    swap(container[i], container[last]);
    return i;
}

template <typename T>
void quick_sort_impl(median_task::utils::Container<T>& container, int first, int last)
{
    if (first < last)
    {
        int i = partition(container, first, last);
        quick_sort_impl(container, first, i - 1);
        quick_sort_impl(container, i + 1, last);
    }
}
}

namespace median_task
{
namespace utils
{
template <typename T>
void quick_sort(Container<T>& container)
{
    quick_sort_impl(container, 0, container.size() - 1);
}

template void quick_sort<int>(Container<int>&);
}
}