#include "Container.hpp"

namespace median_task
{
namespace utils
{
template <typename T>
Container<T>::Container()
{
}

template <typename T>
Container<T>::Container(Container&& other) : mSize(other.mSize), capacity(other.capacity)
{
    data = other.data;
    other.data = nullptr;
    other.mSize = 0;
    other.capacity = 0;
}

template <typename T>
Container<T>::~Container()
{
    delete[] data;
    data = nullptr;
}

template <typename T>
void Container<T>::push_back(T number)
{
    if (data == nullptr && capacity == 0)
    {
        data = new T[++capacity];
    }
    else if (mSize == capacity)
    {
        T* tmp = data;
        capacity = capacity * 2;

        data = new T[capacity];

        for (int i = 0; i < mSize; ++i)
        {
            data[i] = tmp[i];
        }
        delete[] tmp;
        tmp = nullptr;
    }

    data[mSize++] = number;
}

template <typename T>
int Container<T>::size() const
{
    return mSize;
}

template <typename T>
T& Container<T>::operator[](int index)
{
    return data[index];
}

template class Container<float>;
template class Container<int>;
}
}
