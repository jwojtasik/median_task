#pragma once

namespace median_task
{
namespace utils
{
template <typename T>
class Container
{
public:
    Container();
    Container(Container&&);
    Container(const Container&) = delete;
    Container& operator=(const Container&) = delete;
    Container& operator=(Container&&) = delete;
    ~Container();

    void push_back(T item);
    int size() const;
    T& operator[](int index);

private:
    T* data = nullptr;

    int mSize = 0;
    int capacity = 0;
};
}
}
