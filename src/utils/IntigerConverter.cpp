#include "utils/IntigerConverter.hpp"

namespace median_task
{
namespace utils
{
int char_to_int(char* c)
{
    int result = 0;
    int possibleSign = 1;
    if (*c == '-')
    {
        ++c;
        possibleSign = -1;
    }


    while (*c != '\0')
    {
        if (*c >= '0' && *c <= '9')
        {
            result = 10 * result + (*c++ - '0');
        }
        else
        {
            return 0;
        }
    }

    return result * possibleSign;
}
}
}