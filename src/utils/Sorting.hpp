#pragma once

namespace median_task
{
namespace utils
{
template <typename T>
class Container;

template <typename T>
void quick_sort(Container<T>& container);
}
}
