#include <iostream>

#include "utils/Container.hpp"
#include "utils/Solution.hpp"

// input data 3 5 m 8 m 6 m q
int main(int argc, char** argv)
{
    auto result = median_task::utils::solution<float>(argc-1, ++argv);

    for (int i = 0; i < result.size(); ++i)
    {
        std::cout << result[i] << " ";
    }
    return 0;
}

// solution using vector and sort from STL
//#include <algorithm>
//#include <vector>
// int main(int argc, char** argv)
//{
//    std::vector<int> vec(0);
//    for (int i = 1; i < argc; ++i)
//    {
//        if (*argv[i] == 'm')
//        {
//            std::sort(vec.begin(), vec.end());
//            int n = vec.size();
//            if (n % 2 == 0)
//            {
//                std::cout << (vec[n / 2 - 1] + vec[n / 2]) * 0.5 << " ";
//            }
//            else
//                std::cout << vec[n / 2] << " ";
//        }
//        else if (*argv[i] == 'q')
//        {
//            break;
//        }
//        else
//        {
//            vec.emplace_back(std::atoi(argv[i]));
//        }
//    }
//
//    return 0;
//}
