#include <gmock/gmock.h>

#include "utils/Container.hpp"
#include "utils/Sorting.hpp"

namespace median_task
{
namespace tests
{
TEST(SortingTests, SouldPassWithEmptyContainer)
{
    auto container = utils::Container<int>();
    utils::quick_sort(container);
}

TEST(SortingTests, ShouldSortCorrectlyContainerOf3Integers)
{
    auto container = utils::Container<int>();

    container.push_back(45);
    container.push_back(70);
    container.push_back(50);

    ASSERT_THAT(container[container.size() - 1], testing::Eq(50));
    utils::quick_sort(container);

    ASSERT_THAT(container[container.size() - 1], testing::Eq(70));
}

TEST(SortingTests, ShouldSortReversedOrderContainer)
{
    auto container = utils::Container<int>();

    container.push_back(100);
    container.push_back(88);
    container.push_back(77);
    container.push_back(66);
    container.push_back(55);
    container.push_back(44);
    container.push_back(33);

    utils::quick_sort(container);

    for (int i = 0; i < container.size() - 1; ++i)
    {
        ASSERT_THAT(container[i], testing::Le(container[i+1]));
    }
}

TEST(SortingTests, ShouldSortBigRandomContainer)
{
    auto container = utils::Container<int>();

    for (int i = 0; i < 2000; ++i)
    {
        container.push_back(rand());
    }

    utils::quick_sort(container);

    for (int i = 0; i < container.size() - 1; ++i)
    {
        ASSERT_THAT(container[i], testing::Le(container[i + 1]));
    }
}
}
}