#include <gmock/gmock.h>

#include "utils/Container.hpp"

namespace median_task
{
namespace tests
{
TEST(ContainerTests, ShouldCreateContainerWithSize0)
{
    auto container = utils::Container<int>();
    ASSERT_THAT(container.size(), testing::Eq(0));
}

TEST(ContainerTests, ShouldIncreaseSizeAfterPushBack)
{
    auto container = utils::Container<int>();
    container.push_back(5);
    ASSERT_THAT(container.size(), testing::Eq(1));
}

TEST(ContainerTests, ShouldHaveValueAtSecondPositionAfterTwoPush)
{
    auto container = utils::Container<int>();
    container.push_back(5);
    container.push_back(10);
    ASSERT_THAT(container.size(), testing::Eq(2));
    ASSERT_THAT(container[1], testing::Eq(10));
}

TEST(ContainerTests, ShouldHaveValueAtLastPositionAfterTenPush)
{
    auto container = utils::Container<int>();
    container.push_back(1);
    container.push_back(2);
    container.push_back(3);
    container.push_back(4);
    container.push_back(5);
    container.push_back(6);
    container.push_back(7);
    container.push_back(8);
    container.push_back(9);
    container.push_back(99);

    ASSERT_THAT(container.size(), testing::Eq(10));
    ASSERT_THAT(container[container.size()-1], testing::Eq(99));
}
}
}
