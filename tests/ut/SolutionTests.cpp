#include <gmock/gmock.h>

#include "utils/Solution.hpp"

namespace median_task
{
namespace tests
{
TEST(Solution, CheckTaskSolution)
{
    char* args[8] = {"3", "5", "m", "8", "m", "6", "m", "q"};
    auto result = utils::solution<float>(8, args);
    ASSERT_THAT(result[0], testing::Eq(4.f));
    ASSERT_THAT(result[1], testing::Eq(5.f));
    ASSERT_THAT(result[2], testing::Eq(5.5f));
}

TEST(Solution, CheckTaskSolutionWithNegativeNumbers)
{
    char* args[8] = {"-3", "-5", "m", "-8", "m", "-6", "m", "q"};
    auto result = utils::solution<float>(8, args);
    ASSERT_THAT(result.size(), testing::Eq(3));
    ASSERT_THAT(result[0], testing::Eq(-4.f));
    ASSERT_THAT(result[1], testing::Eq(-5.f));
    ASSERT_THAT(result[2], testing::Eq(-5.5f));
}

TEST(Solution, CheckTaskSolutionWithMoreDataAndNegativeNumbers)
{
    char* args[16] = {"3", "5", "m", "8", "m", "6", "m", "38","m", "-10", "m", "-8", "m", "-5", "m", "q"};
    auto result = utils::solution<float>(16, args);
    ASSERT_THAT(result.size(), testing::Eq(7));
    ASSERT_THAT(result[0], testing::Eq(4.f));
    ASSERT_THAT(result[1], testing::Eq(5.f));
    ASSERT_THAT(result[2], testing::Eq(5.5f));
    ASSERT_THAT(result[3], testing::Eq(6.f));
    ASSERT_THAT(result[4], testing::Eq(5.5f));
    ASSERT_THAT(result[5], testing::Eq(5.f));
    ASSERT_THAT(result[6], testing::Eq(4.f));
}
}
}
