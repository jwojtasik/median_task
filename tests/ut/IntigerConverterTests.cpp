#include <gmock/gmock.h>

#include "utils/IntigerConverter.hpp"

namespace median_task
{
namespace tests
{
TEST(IntigerConverter, ConvertsSingleDigitToIntiger)
{
    ASSERT_THAT(utils::char_to_int("6"), testing::Eq(6));
}

TEST(IntigerConverter, ConvertsTwoDigitsToIntiger)
{
    ASSERT_THAT(utils::char_to_int("69"), testing::Eq(69));
}

TEST(IntigerConverter, ShouldHandleNegativeSignCorrectly)
{
    ASSERT_THAT(utils::char_to_int("-69"), testing::Eq(-69));
}

TEST(IntigerConverter, ConvertsSixDigitsToIntiger)
{
    ASSERT_THAT(utils::char_to_int("695468"), testing::Eq(695468));
}

TEST(IntigerConverter, ReturnZeroWhenNotANumber)
{
    ASSERT_THAT(utils::char_to_int("3456abc345"), testing::Eq(0));
}

TEST(IntigerConverter, ReturnZeroWhenEmpty)
{
    ASSERT_THAT(utils::char_to_int(""), testing::Eq(0));
}
}
}